# Colecciones de objetos: localidades, municipios y provincias #

Este programa se ejecuta mediante el comando "make ejecutar". Para más comandos de este programa consulte el makefile encargado de la compilación de fuentes, generación del Javadoc y ejecución del programa. Acompañado del Manifest.txt

### Objetivo de este ejercicio ###

* El propósito del presente ejercicio es introducir el tratamiento de colecciones de objetos. Se pide realizar los apartados que se muestran a continuación:

### Organización ###

Está organizado por las siguientes carpetas:

* Carpeta src encontramos la carpeta dominio y aplicacion.
    * En la carpeta dominio están las clases Localidad(Datos de las localidades), Municipio(Agragación de localidades), Provincia(Agregación de municipios)
    * En la carpeta aplicacion la clase Principal(Se encarga de ejecutar el programa)

* html(javadoc) 
* bin(archivos compilados)

### Datos del autor ###

* Entrega: 26 de Octubre del 2021
* Autor: Álvaro Sanz Cortés