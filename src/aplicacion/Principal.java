package aplicacion;

import dominio.*;

/** La clase Principal se encarga de ejecutar el programa
 * 
 * @author Alvaro Sanz
 */

public class Principal {
    public static void main(String[] args) {

        Localidad local1 = new Localidad();
        local1.setNombre("Madrid metro");
        local1.setNumeroDeHabitantes(500);

        Localidad local2 = new Localidad();
        local2.setNombre("Madrid sur");
        local2.setNumeroDeHabitantes(400);

        Localidad local3 = new Localidad();
        local3.setNombre("Valencia metro");
        local3.setNumeroDeHabitantes(600);

        Localidad local4 = new Localidad();
        local4.setNombre("Valencia sur");
        local4.setNumeroDeHabitantes(400);

        Municipio mun1 = new Municipio();
        mun1.setNombre("Madrid");
        mun1.anadirLocalidad(local1);
        mun1.anadirLocalidad(local2);
        mun1.calcularHabitantesLocalidades();

        Municipio mun2 = new Municipio();
        mun2.setNombre("Valencia");
        mun2.anadirLocalidad(local3);
        mun2.anadirLocalidad(local3);
        mun2.calcularHabitantesLocalidades();

        Provincia prov1 = new Provincia();
        prov1.setNombre("Comunidad de Madrid");
        prov1.addMunicipios(mun1);
        prov1.addMunicipios(mun2);
        prov1.calcularHabitantesMunicipios();

        System.out.println(prov1);
    }
}
