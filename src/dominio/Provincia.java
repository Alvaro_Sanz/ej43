package dominio;

import java.util.ArrayList;

/** La clase Provincia, que es una gragación de las municipios, se define con su nombre y  un método para calcular el número total de municipios 
 * 
 * @author Alvaro Sanz Cortes
 */

public class Provincia {
    private String nombre;
    private ArrayList<Municipio> municipios = new ArrayList<>();

   /** Devuelve el nombre de la clase Municipio
    * 
    * @return nombre
    */

    public String getNombre(){
        return nombre;
    }

    /** Devuelve el nuevo nombre de la clase Muncipio
    * 
    * @param nombre nuevo nombre
    */

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    /** Metodo que añade los municipios a una ArrayList de la clase Provincia
    * 
    * @param municipio municipio
    */

    public void addMunicipios(Municipio municipio){
        municipios.add(municipio);
    }

    /** Método que calcula en numero total de habitantes de los municipios añadidas a la ArrayList de la clase Provincia
    * 
    * @return habitantesMunicipio
    */

    public int calcularHabitantesMunicipios(){
        int habitantesMunicipio = 0;
        for (int i = 0; i < municipios.size(); i ++){ 
            habitantesMunicipio += municipios.get(i).calcularHabitantesLocalidades();
        }
        return habitantesMunicipio;
    }

    /** Devuelve la información de la clase Localidad
     * 
     * @return mensaje
    */

    public String toString(){
        String mensaje = "Provincia " + nombre + " tiene " + calcularHabitantesMunicipios() + " habitantes.\n";
        for (int i = 0; i < municipios.size(); i++) {
            mensaje += municipios.get(i);
        }
        return mensaje;
    }
}