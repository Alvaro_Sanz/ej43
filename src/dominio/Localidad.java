package dominio;

/** La clase Localidad se define con su nombre y el número de habitantes 
 * 
 * @author Alvaro Sanz Cortes
 */

public class Localidad {
    private String nombre;
    private int numeroDeHabitantes;

    /** Devuelve el nombre de la Localidad
     * 
     * @return nombre
    */

    public String getNombre(){
        return nombre;
    }

    /** Devuelve el nuevo nombre de la Localidad
     * 
     * @param nombre nuevo nombre
    */

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    /** Devuelve el numero de habitantes de la Localidad
     * 
     * @return numeroDeHabitantes
    */

    public int getNumeroDeHabitantes(){
        return numeroDeHabitantes;
    }

    /** Devuelve el nuevo numero de habbitantes de la Localidad
     * 
     * @param nHab nuevo numeroDeHabitantes
     */

    public void setNumeroDeHabitantes(int nHab){
        numeroDeHabitantes = nHab;
    }

    /** Devuelve la información de la clase Localidad
     * 
     * @return nombre y numeroDeHabitantes
     */

    public String toString(){
        return "La localidad " + nombre + " tiene " + numeroDeHabitantes + " habitantes.";
    }
}
