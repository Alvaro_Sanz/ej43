package dominio;

import java.util.ArrayList;

/** La clase Municipio, que es una gragación de las localidades, se define con su nombre y  un método para calcular el número total de habitantes 
 * 
 * @author Alvaro Sanz Cortes
 */

public class Municipio {
   private String nombre;
   private ArrayList<Localidad> localidades = new ArrayList<>();

   /** Devuelve el nombre de la clase Municipio
    * 
    * @return nombre
    */

   public String getNombre(){
       return nombre;
   }

   /** Devuelve el nuevo nombre de la clase Muncipio
    * 
    * @param nombre nuevo nombre
    */

   public void setNombre(String nombre){
       this.nombre = nombre;
   }

   /** Metodo que añade las localidades a una ArrayList de la clase Municipio
    * 
    * @param localidad Localidad
    */

   public void anadirLocalidad(Localidad localidad){
       localidades.add(localidad);
   }

   /** Método que calcula en numero total de habitantes de las localidades añadidas a la ArrayList de la clase Municipio
    * 
    * @return habitantesMunicipio
    */

   public int calcularHabitantesLocalidades(){
       int habitantesMunicipio = 0;
       for (int i = 0; i < localidades.size(); i++){
            habitantesMunicipio += localidades.get(i).getNumeroDeHabitantes();
        }
    return habitantesMunicipio;
    }

    /** Devuelve la información de la clase Localidad
     * 
     * @return mensaje
     */

   public String toString(){
       String mensaje = "El municipio " + nombre + " tiene " + calcularHabitantesLocalidades() + " habitantes.";

       for (int i = 0; i < localidades.size(); i++) {
           mensaje += "\n" + localidades.get(i);
       }
		return "\n" + mensaje;
	}
}
